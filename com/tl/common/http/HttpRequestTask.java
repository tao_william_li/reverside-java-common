package com.tl.common.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import android.os.AsyncTask;

public class HttpRequestTask extends AsyncTask<String, Void, String>
{

   private static final String CHARSET = "UTF-8";

   private HttpResponseListener mResponseListener;

   private String mURL;

   private String mRequestParamName;
   private String mGsonRequest;

   public HttpRequestTask(HttpResponseListener listener, String url,
         String inRequestParamName, String inGsonRequest)
   {
      mResponseListener = listener;
      mURL = url;
      mRequestParamName = inRequestParamName;
      mGsonRequest = inGsonRequest;
   }

   @Override
   protected String doInBackground(String... urls)
   {
      return getOutputFromUrl();
   }

   private String getOutputFromUrl()
   {
      String output = new String();
      try
      {
         String query = String.format(mRequestParamName + "=%s",
               URLEncoder.encode(mGsonRequest, CHARSET));

         URLConnection connection = new URL(mURL).openConnection();
         connection.setDoOutput(true); // Triggers POST.
         connection.setRequestProperty("Accept-Charset", CHARSET);
         connection.setRequestProperty("Content-Type",
               "application/x-www-form-urlencoded;charset=" + CHARSET);

         OutputStream connectionOut = connection.getOutputStream();
         connectionOut.write(query.getBytes(CHARSET));

         BufferedReader bufferedReader = new BufferedReader(
               new InputStreamReader(connection.getInputStream()));

         String line;

         // read from the urlconnection via the bufferedreader
         while ((line = bufferedReader.readLine()) != null)
         {
            output += line;
         }
         bufferedReader.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return output;
   }

   @Override
   protected void onPreExecute()
   {
      if (mResponseListener != null)
      {
         mResponseListener.onPrepareResponse();
      }
   }

   @Override
   protected void onPostExecute(String output)
   {
      if (mResponseListener != null)
      {
         mResponseListener.onResponse(output);
      }
   }
}
