package com.tl.common.http;

public interface HttpResponseListener
{
   public void onPrepareResponse();
   public void onResponse(String json);
}
