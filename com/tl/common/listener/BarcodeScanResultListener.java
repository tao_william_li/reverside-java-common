package com.tl.common.listener;

public interface BarcodeScanResultListener
{
   public void onBarcodeScanSuccess(String barcode);
   public void onBarcodeScanFailed();
}
