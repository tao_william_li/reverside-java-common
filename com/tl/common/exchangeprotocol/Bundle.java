/*
 * The common data exchange structure.
 * 
 *   Client A                               Client B
 * 
 * |----------|                           |----------|
 * |  Bundle  |                           |  Bundle  |
 * |----------|                           |----------|
 *      ↓(Convert to Json)                     ↑(Convert from Json)
 * |----------|                           |----------|
 * |  String  |                           |  String  |
 * |----------|                           |----------|
 *      ↓                                       ↑
 *       → →  (Send via HTTP, TCP/IP, etc.)  → → 
 */

package com.tl.common.exchangeprotocol;

import java.util.HashMap;
import java.util.Map;

public class Bundle
{
	private Map<String, Object> data;

	public Bundle()
	{
		data = new HashMap<String, Object>();
	}

	public void add(String key, Object obj)
	{
		data.put(key, obj);
	}

	public Object getData(String key)
	{
		return data.get(key);
	}

	public void setData(Map<String, Object> data)
	{
		this.data = data;
	}
}
