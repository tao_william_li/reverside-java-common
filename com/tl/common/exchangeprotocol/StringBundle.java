package com.tl.common.exchangeprotocol;

import java.util.HashMap;
import java.util.Map;

public class StringBundle
{
   private Map<String, String> data;
   
   public StringBundle()
   {
      data = new HashMap<String, String>();
   }

   public void add(String key, String obj)
   {
      data.put(key, obj);
   }

   public String getData(String key)
   {
      return data.get(key);
   }

   public void setData(Map<String, String> data)
   {
      this.data = data;
   }
}
