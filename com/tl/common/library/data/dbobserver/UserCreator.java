package com.tl.common.library.data.dbobserver;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tl.common.db.DBObserver;
import com.tl.common.library.data.User;

public class UserCreator implements DBObserver
{
   private User user;

   public User getUser()
   {
      return user;
   }

   public void setUser(User user)
   {
      this.user = user;
   }

   public UserCreator(User user)
   {
      super();
      this.user = user;
   }

   public void update(int count)
   {
   }

   public void update(ResultSet results)
   {
      // id, name, password
      try
      {
         if (results.next())
         {
            user.setId(results.getInt(1));
            user.setName(results.getString(2));
            user.setPassword(results.getString(3));
         }
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

}
