package com.tl.common.library.data.dbobserver;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tl.common.db.DBObserver;
import com.tl.common.library.data.Reader;

public class ReaderCreator implements DBObserver
{
   private Reader reader;

   public ReaderCreator(Reader reader)
   {
      this.reader = reader;
   }

   public Reader getStudent()
   {
      return reader;
   }

   public void setStudent(Reader student)
   {
      this.reader = student;
   }

   public void update(int count)
   {
   }

   public void update(ResultSet results)
   {
      // id, code_id, name
      try
      {
         if (results.next())
         {
            reader.setId(results.getInt(1));
            reader.setBarcode(results.getString(2));
            reader.setName(results.getString(3));
         }
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

}
