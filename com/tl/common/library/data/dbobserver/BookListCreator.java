package com.tl.common.library.data.dbobserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.tl.common.db.DBObserver;
import com.tl.common.library.data.Book;

public class BookListCreator implements DBObserver
{
   private List<Book> books;

   public BookListCreator(List<Book> books)
   {
      this.books = books;
   }

   public List<Book> getBooks()
   {
      return books;
   }

   public void setBooks(List<Book> books)
   {
      this.books = books;
   }

   public void update(int count)
   {
      // TODO Auto-generated method stub

   }

   public void update(ResultSet results)
   {
      // id, barcode, name, price
      try
      {
         while(results.next())
         {
            books.add(new Book(results.getInt(1), results.getString(2), results.getString(3), results.getDouble(4)));
         }
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

}
