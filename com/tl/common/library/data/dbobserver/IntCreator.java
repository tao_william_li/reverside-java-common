package com.tl.common.library.data.dbobserver;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tl.common.db.DBObserver;

public class IntCreator implements DBObserver
{
	private int value = 0;

	public int getValue()
	{
		return value;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	public void update(int count)
	{
	}

	public void update(ResultSet results)
	{
		try
		{
			if(results.next())
			{
				value = results.getInt(1);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
