package com.tl.common.library.data.dbobserver;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tl.common.db.DBObserver;
import com.tl.common.library.data.Book;

public class BookCreator implements DBObserver
{
   private Book book;

   public BookCreator(Book book)
   {
      super();
      this.book = book;
   }

   public Book getBook()
   {
      return book;
   }

   public void setBook(Book book)
   {
      this.book = book;
   }

   public void update(int count)
   {
   }

   public void update(ResultSet results)
   {
      // id, barcode, name, price
      try
      {
         if(results.next())
         {
            book.setId(results.getInt(1));
            book.setBarcode(results.getString(2));
            book.setName(results.getString(3));
            book.setPrice(results.getDouble(4));
         }
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

}
