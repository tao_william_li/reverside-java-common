package com.tl.common.library.data;

public class Reader
{
   private int id;
   private String barcode;
   private String name;

   public Reader(int id, String barcode, String name)
   {
      this.id = id;
      this.barcode = barcode;
      this.name = name;
   }

   public String getBarcode()
   {
      return barcode;
   }

   public void setBarcode(String barcode)
   {
      this.barcode = barcode;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }
}
