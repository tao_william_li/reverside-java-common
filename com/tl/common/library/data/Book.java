package com.tl.common.library.data;

public class Book
{
   private int id;
   private String barcode;
   private String name;
   private double price;
   
   public Book(int id, String barcode, String name, double price)
   {
      this.id = id;
      this.barcode = barcode;
      this.name = name;
      this.price = price;
   }

   public String getBarcode()
   {
      return barcode;
   }

   public void setBarcode(String barcode)
   {
      this.barcode = barcode;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public double getPrice()
   {
      return price;
   }

   public void setPrice(double price)
   {
      this.price = price;
   }

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }
}
