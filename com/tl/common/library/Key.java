package com.tl.common.library;

public class Key
{
   //public static String Server = "http://192.168.10.100:8080/library-ws";
   public static String Server = "https://lib-zbps.rhcloud.com";
   public static String ServletLogin = Server + "/Login";
   public static String ServletGetReaderInfo = Server + "/GetReaderInfo";
   public static String ServletGetBookInfo = Server + "/GetBookInfo";
   public static String ServletBorrowBook = Server + "/BorrowBook";
   public static String ServletReturnBook = Server + "/ReturnBook";
   public static String ServletStoreNewBook = Server + "/StoreNewBook";
   public static String ServletUpdateBook = Server + "/UpdateBook";
   public static String ServletGetBorrowAllowance = Server + "/GetBorrowAllowance";
   public static int STUDENT_CODE_LENGTH = 9;
   public enum value
   {
      Login,
      Operation,
      User,
      Reader,
      Book,
      Books,
      Info,
      BorrowAllowance,
      Barcode,
      Borrow,
      Return,
      NewBook,
      UpdateBook,
   }
}
