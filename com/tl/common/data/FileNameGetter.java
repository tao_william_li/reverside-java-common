package com.tl.common.data;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

public class FileNameGetter
{
	public List<String> names;

	public FileNameGetter(List<String> names)
	{
		this.names = names;
	}

	public void get(String path)
	{
		Path p = Paths.get(path);
		SimpleFileVisitor<Path> fv = new SimpleFileVisitor<Path>()
		{

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			      throws IOException
			{
				names.add(file.toString());
				return FileVisitResult.CONTINUE;
			}
		};
		try
		{
			Files.walkFileTree(p, fv);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
