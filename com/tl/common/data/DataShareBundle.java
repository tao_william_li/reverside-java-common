package com.tl.common.data;

import com.tl.common.exchangeprotocol.Bundle;

public class DataShareBundle
{
   private static DataShareBundle instance;

   private Bundle bundle;

   private DataShareBundle()
   {
      bundle = new Bundle();
   }

   public static DataShareBundle instance()
   {
      if (instance == null)
      {
         instance = new DataShareBundle();
      }
      return instance; 
   }

   public Bundle getBundle()
   {
      return bundle;
   }
}
