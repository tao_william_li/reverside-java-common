package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBStatementValueString implements DBStatementValue
{
	private String m_value;

	public DBStatementValueString(String value)
	{
		m_value = value;
	}

	public void setValue(PreparedStatement statement, int index) throws SQLException
	{
		statement.setString(index, m_value);
	}
}
