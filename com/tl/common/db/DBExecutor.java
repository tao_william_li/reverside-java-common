package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface DBExecutor
{
	public void execute(PreparedStatement ps, DBObserver obsever) throws SQLException;
}
