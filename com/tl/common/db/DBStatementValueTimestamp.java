package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class DBStatementValueTimestamp implements DBStatementValue
{

	private Timestamp m_value;

	public DBStatementValueTimestamp(Date value)
	{
		m_value = new Timestamp(value.getTime());
	}

	public void setValue(PreparedStatement statement, int index)
	      throws SQLException
	{
		statement.setTimestamp(index, m_value);
	}
}
