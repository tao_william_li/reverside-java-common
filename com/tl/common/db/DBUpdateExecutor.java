package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBUpdateExecutor implements DBExecutor
{
	public void execute(PreparedStatement ps, DBObserver obsever)
	      throws SQLException
	{
		System.out.println("Executing SQL: " + ps.toString());
		if(obsever != null)
		{
			obsever.update(ps.executeUpdate());
		}
		ps.close();
	}
}
