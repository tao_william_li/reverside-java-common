package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBStatementValueBoolean implements DBStatementValue
{
	private boolean m_value;

	public DBStatementValueBoolean(boolean value)
	{
		m_value = value;
	}

	public void setValue(PreparedStatement statement, int index)
	      throws SQLException
	{
		statement.setBoolean(index, m_value);
	}

}
