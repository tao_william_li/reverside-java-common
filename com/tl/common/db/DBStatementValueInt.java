package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBStatementValueInt implements DBStatementValue
{
	public int m_value;

	public DBStatementValueInt(int value)
	{
		m_value = value;
	}

	public void setValue(PreparedStatement statement, int index)
	      throws SQLException
	{
		statement.setInt(index, m_value);
	}
}
