/*
 * Business class
 * Execute tasks that managed by DBManager.
 */

package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

public class DBThread extends Thread
{
	private List<DBTask> m_tasks;
	private DBConfig config;

	public DBThread(List<DBTask> tasks, DBConfig config)
	{
		m_tasks = tasks;
		this.config = config;
	}

	public void run()
	{
		while(true)
		{
			synchronized(m_tasks)
			{
				if(!m_tasks.isEmpty())
				{
					DBTask obj = m_tasks.get(0);
					System.out.println("Now executing " + obj.m_sql);
					try
					{
						PreparedStatement ps = DBManager.instance(config).getConnection()
						      .prepareStatement(obj.m_sql);
						if(obj.m_values != null)
						{
							Iterator<DBStatementValue> it = obj.m_values.iterator();
							int index = 1;
							while(it.hasNext())
							{
								DBStatementValue value = it.next();
								value.setValue(ps, index++);
							}
						}
						obj.m_dbExecutor.execute(ps, obj.m_obsever);
					}
					catch(SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					m_tasks.remove(obj);
				}
			}
		}
	}
}
