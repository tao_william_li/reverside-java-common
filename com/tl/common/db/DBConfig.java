package com.tl.common.db;

public interface DBConfig
{
   public String host();
   public String port();
   public String user();
   public String password();
   public String db();
}
