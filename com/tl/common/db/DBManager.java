/*
 * Business class
 * Store tasks, use DBTaskExecutor to execute these tasks.
 */

package com.tl.common.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class DBManager
{
	private static DBManager m_manager;
	private List<DBTask> m_tasks;
	private DBThread m_dbThread;
	private Connection m_connection;

	private DBConfig config;

	private DBManager(DBConfig config)
	{
		this.config = config;
	}

	public static DBManager instance(DBConfig config)
	{
		if(m_manager == null)
		{
			m_manager = new DBManager(config);
			m_manager.init();
		}
		else
		{
			try
			{
				if(!m_manager.isValid())
				{
					m_manager.m_connection.close();
					m_manager.openConnection();
				}
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return m_manager;
	}

	public void addAsynchronousTask(String sql, List<DBStatementValue> args,
	      DBExecutor dbExecutor, DBObserver observer)
	{
		synchronized(m_tasks)
		{
			m_tasks.add(new DBTask(sql, args, dbExecutor, observer));
		}
	}

	private void init()
	{
		m_tasks = new LinkedList<DBTask>();
		m_dbThread = new DBThread(m_tasks, config);
		openConnection();
	}

	private void openConnection()
	{
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://" + config.host() + ":" + config.port() + "/"
		      + config.db() + "?useUnicode=true&characterEncoding=UTF-8";
		System.out.println(url);
		System.out.println(config.user());
		System.out.println(config.password());
		try
		{
			Class.forName(driver);
			m_connection = DriverManager.getConnection(url, config.user(),
			      config.password());
			if(!m_connection.isClosed())
			{
				System.out.println("Succeeded connecting to the Database!");
			}
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Sorry,can`t find the Driver!");
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void executeAsynchronousTask()
	{
		m_dbThread.start();
	}

	public void executeTask(String sql, List<DBStatementValue> args,
	      DBExecutor dbExecutor, DBObserver observer)
	{
		if(m_connection != null)
		{
			try
			{
				PreparedStatement ps = m_connection.prepareStatement(sql);
				if(args != null)
				{
					Iterator<DBStatementValue> it = args.iterator();
					int index = 1;
					while(it.hasNext())
					{
						DBStatementValue value = it.next();
						value.setValue(ps, index++);
					}
				}
				dbExecutor.execute(ps, observer);
			}
			catch(SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Connection getConnection()
	{
		return m_connection;
	}

	private boolean isValid()
	{
		boolean valid = false;
		if(m_connection != null)
		{
			try
			{
				PreparedStatement ps = getConnection()
				      .prepareStatement("SELECT 1;");
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				{
					valid = true;
				}
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return valid;
	}
}