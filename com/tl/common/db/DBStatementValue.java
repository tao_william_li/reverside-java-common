package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface DBStatementValue
{
	public void setValue(PreparedStatement statement, int index) throws SQLException;
}
