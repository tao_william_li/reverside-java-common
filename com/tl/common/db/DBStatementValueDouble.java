package com.tl.common.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBStatementValueDouble implements DBStatementValue
{
	public double m_value;

	public DBStatementValueDouble(double value)
	{
		m_value = value;
	}

	public void setValue(PreparedStatement statement, int index)
	      throws SQLException
	{
		statement.setDouble(index, m_value);
	}

}
