package com.tl.common.db;

import java.util.List;

public class DBTask
{
	public String m_sql;
	public List<DBStatementValue> m_values;
	public DBExecutor m_dbExecutor;
	public DBObserver m_obsever;

	public DBTask(String sql, List<DBStatementValue> values, DBExecutor dbExecutor, DBObserver obsever)
	{
		m_sql = sql;
		m_values = values;
		m_dbExecutor = dbExecutor;
		m_obsever = obsever;
	}
}
