package com.tl.common.db;

import java.sql.ResultSet;

public class DBAffectedLineCountObserver implements DBObserver
{
	private int lineCount = 0;

	public void update(int count)
	{
		lineCount += count;
	}

	public int getLineCount()
	{
		return lineCount;
	}

	public void update(ResultSet results)
	{
		// TODO Auto-generated method stub

	}

}
