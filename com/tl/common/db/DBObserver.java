package com.tl.common.db;

import java.sql.ResultSet;

public interface DBObserver
{
	public void update(int count);
	public void update(ResultSet results);
}
