package com.tl.common.postman;

public class ParameterKeyManager
{
	public static String LOGIN = "login";
	public static String GET_POSTMAN_TASK_LIST = "get_postman_task_list";
	public static String HIRE_POSTMAN = "hire_postman";
	public static String ADMIN = "admin";
	public static String POSTMAN = "postman";
	public static String SHIPMENT = "shipment";
	public static String NEW_POSTMAN = "new_postman";
	public static String FIRE_POSTMAN = "fire_postman";
	public static String PAST_POSTMAN = "past_postman";
	public static String STORE_NEW_SHIPMENT = "store_new_shipment";
	public static String STORE_NEW_SHIPMENTS = "store_new_shipments";
	public static String ASSIGN_SHIPMENT = "assign_shipment";
	public static String ASSIGNED = "assigned";
	public static String SELECTED_SHIPMENT = "selected_shipment";
	public static String DELIVER_SHIPMENT = "deliver_shipment";
	public static String GET_UNASSIGNED_TASK_OF_SECTION = "get_unassigned_task_of_section";
	public static String GET_SECTIONS = "get_sections";
	public static String GET_POSTMAN_LIST = "get_postman_list";
	public static String NEXT_ACTIVITY_NAME = "next_activity_name";
	public static String SECTION = "section";
	public static String SUCCESS = "success";
	public static String FAILURE = "failure";
	public static String BARCODE = "barcode";
	public static String BARCODE_POSITION = "barcode_postion";

	public static String SERVLET_URL = "https://ws-riverside.rhcloud.com";
	//public static String SERVLET_URL = "http://192.168.1.100:8080/ws";
	public static String SERVLET_URL_LOGIN = SERVLET_URL + "/Login";
	public static String SERVLET_URL_GET_POSTMAN_TASK_LIST = SERVLET_URL + "/GetPostmanOpenTaskList";
	public static String SERVLET_URL_DELIVER_SHIPMENT = SERVLET_URL + "/DeliverShipment";
	public static String SERVLET_URL_STORE_NEW_SHIPMENT = SERVLET_URL + "/StoreNewShipment";
	public static String SERVLET_URL_STORE_NEW_SHIPMENTS = SERVLET_URL + "/StoreNewShipments";
	public static String SERVLET_URL_GET_SECTIONS = SERVLET_URL + "/GetSections";
	public static String SERVLET_URL_GET_UNASSIGNED_TASK_OF_SECTION = SERVLET_URL + "/GetUnassignedTaskOfSection";
	public static String SERVLET_URL_GET_POSTMAN_LIST = SERVLET_URL + "/GetPostmanList";
	public static String SERVLET_URL_ASSIGN_SHIPMENTT = SERVLET_URL + "/AssignShipment";
}
