package com.tl.common.postman;

import java.sql.Timestamp;

public class Shipment
{
	private String mBarcodeKey;
	private String mReceiver;
	private String mPhone;
	private Timestamp mDeliveryDate;
	private int mSection;

	public Shipment(String barcodeKey, String receiver, String phone,
	      Timestamp deliveryDate, int section)
	{
		this.mBarcodeKey = barcodeKey;
		this.mReceiver = receiver;
		this.mPhone = phone;
		this.mSection = section;
		this.mDeliveryDate = deliveryDate;
	}

	public String getBarcodeKey()
	{
		return mBarcodeKey;
	}

	public void setBarcodeKey(String barcodeKey)
	{
		this.mBarcodeKey = barcodeKey;
	}

	public String getReceiver()
	{
		return mReceiver;
	}

	public void setReceiver(String receiver)
	{
		this.mReceiver = receiver;
	}

	public String getPhone()
	{
		return mPhone;
	}

	public void setPhone(String phone)
	{
		this.mPhone = phone;
	}

	public Timestamp getDeliveryDate()
	{
		return mDeliveryDate;
	}

	public void setDeliveryDate(Timestamp deliveryDate)
	{
		this.mDeliveryDate = deliveryDate;
	}

	public int getSection()
	{
		return mSection;
	}

	public void setSection(int section)
	{
		this.mSection = section;
	}
}
