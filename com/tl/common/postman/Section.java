package com.tl.common.postman;

public class Section
{
	private int mID;
	private String mName;

	public Section(int id, String name)
	{
		this.mID = id;
		this.mName = name;
	}

	public int getID()
	{
		return mID;
	}

	public void setID(int id)
	{
		this.mID = id;
	}

	public String getName()
	{
		return mName;
	}

	public void setName(String name)
	{
		this.mName = name;
	}
}
