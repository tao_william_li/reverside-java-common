package com.tl.common.postman.exchangeprotocol;

import java.util.List;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Shipment;

public class AssignShipmentProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAssigner;
	private Postman mAssignee;
	private List<Shipment> mShipments;
	private int mEffectedLine = 0;
	
	public AssignShipmentProtocol(Postman assigner, Postman assignee, List<Shipment> shipments)
	{
		mAssigner = assigner;
		mAssignee = assignee;
		mShipments = shipments;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public int getEffectedLine()
	{
		return mEffectedLine;
	}

	public void setEffectedLine(int effectedLine)
	{
		this.mEffectedLine = effectedLine;
	}

	public Postman getAssigner()
	{
		return mAssigner;
	}

	public void setAssigner(Postman assigner)
	{
		this.mAssigner = assigner;
	}

	public Postman getAssignee()
	{
		return mAssignee;
	}

	public void setAssigne(Postman assignee)
	{
		this.mAssignee = assignee;
	}

	public List<Shipment> getShipments()
	{
		return mShipments;
	}

	public void setShipments(List<Shipment> shipments)
	{
		this.mShipments = shipments;
	}
}
