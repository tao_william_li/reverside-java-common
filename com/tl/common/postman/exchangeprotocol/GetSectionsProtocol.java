package com.tl.common.postman.exchangeprotocol;

import java.util.List;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Section;

public class GetSectionsProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAdmin;
	private List<Section> mSections;

	public GetSectionsProtocol(Postman admin, List<Section> sections)
	{
		mAdmin = admin;
		mSections = sections;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getAdmin()
	{
		return mAdmin;
	}

	public void setAdmin(Postman admin)
	{
		this.mAdmin = admin;
	}

	public List<Section> getSections()
	{
		return mSections;
	}

	public void setSectoins(List<Section> sections)
	{
		this.mSections = sections;
	}
}
