package com.tl.common.postman.exchangeprotocol;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;

public class LoginProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mPostman;

	public LoginProtocol(Postman postman)
	{
		mPostman = postman;
	}
	
	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getPostman()
	{
		return mPostman;
	}

	public void setPostman(Postman postman)
	{
		this.mPostman = postman;
	}
}
