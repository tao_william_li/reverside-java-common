package com.tl.common.postman.exchangeprotocol;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Shipment;

public class StoreNewShipmentProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAdmin;
	private Shipment mShipment;
	private int mEffectedLine = 0;

	public StoreNewShipmentProtocol(Postman admin, Shipment shipment)
	{
		mAdmin = admin;
		mShipment = shipment;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getAdmin()
	{
		return mAdmin;
	}

	public void setAdmin(Postman admin)
	{
		this.mAdmin = admin;
	}

	public Shipment getShipment()
	{
		return mShipment;
	}

	public void setShipment(Shipment shipment)
	{
		this.mShipment = shipment;
	}

	public int getEffectedLine()
	{
		return mEffectedLine;
	}

	public void setEffectedLine(int effectedLine)
	{
		this.mEffectedLine = effectedLine;
	}
}
