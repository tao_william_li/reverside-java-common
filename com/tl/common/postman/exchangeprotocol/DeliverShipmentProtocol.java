package com.tl.common.postman.exchangeprotocol;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Shipment;

public class DeliverShipmentProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mPostman;
	private Shipment mShipment;
	private int mEffectedLine = 0;
	
	public DeliverShipmentProtocol(Postman postman, Shipment shipment)
	{
		mPostman = postman;
		mShipment = shipment;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public int getEffectedLine()
	{
		return mEffectedLine;
	}

	public void setEffectedLine(int effectedLine)
	{
		this.mEffectedLine = effectedLine;
	}

	public Postman getPostman()
	{
		return mPostman;
	}

	public void setPostman(Postman postman)
	{
		this.mPostman = postman;
	}

	public Shipment getShipment()
	{
		return mShipment;
	}

	public void setShipment(Shipment shipment)
	{
		this.mShipment = shipment;
	}
}
