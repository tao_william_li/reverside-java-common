package com.tl.common.postman.exchangeprotocol;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;

public class HirePostmanProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAdmin;
	private Postman mNewPostman;

	public HirePostmanProtocol(Postman admin, Postman postman)
	{
		mAdmin = admin;
		mNewPostman = postman;
	}
	
	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getAdmin()
	{
		return mAdmin;
	}

	public void setAdmin(Postman admin)
	{
		this.mAdmin = admin;
	}

	public Postman getPostman()
	{
		return mNewPostman;
	}

	public void setPostman(Postman postman)
	{
		this.mNewPostman = postman;
	}

}
