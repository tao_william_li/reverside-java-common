package com.tl.common.postman.exchangeprotocol;

import java.util.List;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;

public class GetPostmanListProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAdmin;
	private List<Postman> mPostmanList;

	public GetPostmanListProtocol(Postman admin, List<Postman> postmanList)
	{
		mAdmin = admin;
		mPostmanList = postmanList;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getAdmin()
	{
		return mAdmin;
	}

	public void setAdmin(Postman admin)
	{
		this.mAdmin = admin;
	}

	public List<Postman> getPostmanList()
	{
		return mPostmanList;
	}

	public void setPostmanList(List<Postman> postmanList)
	{
		this.mPostmanList = postmanList;
	}
}
