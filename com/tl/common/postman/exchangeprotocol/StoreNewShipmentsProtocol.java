package com.tl.common.postman.exchangeprotocol;

import java.util.List;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Shipment;

public class StoreNewShipmentsProtocol
{
	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mAdmin;
	private List<Shipment> mShipments;
	private int mEffectedLine = 0;

	public StoreNewShipmentsProtocol(Postman admin, List<Shipment> shipments)
	{
		mAdmin = admin;
		mShipments = shipments;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getAdmin()
	{
		return mAdmin;
	}

	public void setAdmin(Postman admin)
	{
		this.mAdmin = admin;
	}

	public List<Shipment> getShipments()
	{
		return mShipments;
	}

	public void setShipments(List<Shipment> shipments)
	{
		this.mShipments = shipments;
	}

	public int getEffectedLine()
	{
		return mEffectedLine;
	}

	public void setEffectedLine(int effectedLine)
	{
		this.mEffectedLine = effectedLine;
	}
}
