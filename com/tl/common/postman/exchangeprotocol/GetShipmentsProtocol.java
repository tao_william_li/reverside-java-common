package com.tl.common.postman.exchangeprotocol;

import java.util.List;

import com.tl.common.postman.ParameterKeyManager;
import com.tl.common.postman.Postman;
import com.tl.common.postman.Shipment;

public class GetShipmentsProtocol
{

	// ParameterKeyManager.SUCCESS or ParameterKeyManager.FAILURE
	private String mStatus = ParameterKeyManager.FAILURE;
	// Data
	private Postman mPostman;
	private List<Shipment> mShipments;

	public GetShipmentsProtocol(Postman postman, List<Shipment> shipments)
	{
		mPostman = postman;
		mShipments = shipments;
	}

	public String getStatus()
	{
		return mStatus;
	}

	public void setStatus(String status)
	{
		this.mStatus = status;
	}

	public Postman getPostman()
	{
		return mPostman;
	}

	public void setPostman(Postman postman)
	{
		this.mPostman = postman;
	}

	public List<Shipment> getShipments()
	{
		return mShipments;
	}

	public void setShipments(List<Shipment> shipments)
	{
		this.mShipments = shipments;
	}
}
