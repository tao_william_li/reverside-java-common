package com.tl.common.postman;

public class Postman
{
	private String mName;
	private String mPassword;
	private String mPhone;
	private int mSection;
	private boolean mIsValid = false;
	private String mRights = new String();

	public Postman(String name, String password, String phone, int section)
	{
		this.mName = name;
		this.mPassword = password;
		this.mPhone = phone;
		this.mSection = section;
	}

	public String getName()
	{
		return mName;
	}

	public void setName(String name)
	{
		this.mName = name;
	}

	public String getPassword()
	{
		return mPassword;
	}

	public void setPassword(String password)
	{
		this.mPassword = password;
	}

	public String getPhone()
	{
		return mPhone;
	}

	public void setPhone(String phone)
	{
		this.mPhone = phone;
	}

	public boolean isValid()
	{
		return mIsValid;
	}

	public void setValidation(boolean isValid)
	{
		this.mIsValid = isValid;
	}

	public String getRights()
   {
	   return mRights;
   }

	public void setRights(String rights)
   {
	   this.mRights = rights;
	}

	public int getSection()
	{
		return mSection;
	}

	public void setSection(int section)
	{
		this.mSection = section;
	}
}
