package com.tl.common.food;

public class Key
{
	enum Func
	{
		UserSignUp,
		UserLogin,
		UserLogout,
		UserChangePasword,

		UserSetIsShareGeo, // Restaurant will know where user is when user turn on share Geo.
		
		UserReserve,
		UserViewReservation,
		UserSendMessage2Restaurant,
		UserUpdateEstimateDateOfArrive,
		UserArrive,
		UserPay,
		UserRateRestaurant,

		UserGetRestaurantListByRate,
		UserGetRestaurantListByGeo,
		UserGetRestaurantListByCategory,
		UserGetRestaurantListByFilter, // sweet, spicy
		
		RestaurantSignUp,
		RestaurantLogin,
		RestaurantUserLogout,
		RestaurantChangePasword,
		
		RestaurantSendMessage2User,
		RestaurantViewReservation, // Show table reserved, customer distance if customer turn on Geo share.
		
		RestaurantGetLayout,
		RestaurantUpdateLayout,
	}
}
